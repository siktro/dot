#!/usr/bin/env bash

packages=(
  # terminal, prompt & stuff
  tmux
  fish

  # apps
  git
  neovim
  ranger

  # cli tools
  fzf
  bat
  ripgrep
  exa
)

function install_nix
{
  # install nix package manager
  if ! command -v "nix-env" &> /dev/null; then
    echo "nix package manager is not found; installing..."
    curl -L https://nixos.org/nix/install | sh
    . ~/.nix-profile/etc/profile.d/nix.sh
  fi
}

function install_packages
{
  for pack in ${packages[@]}; do
    if ! nix-env -q "$pack" --installed > /dev/null; then
      # install package
      nix-env -iA "nixpkgs.$pack"
    fi
  done
}

function change_default_shell 
{
  local exec="$(command -v fish)"

  if ! grep -Fxq "$exec" /etc/shells; then
    echo "$exec" >> /etc/shells
  fi

  if [[ "$exec" != "$SHELL" ]]; then
    sudo chsh -s $(exec) $USER
  fi
}

function main
{
  install_nix
  install_packages
  change_default_shell 
}

main
